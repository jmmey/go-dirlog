package main

//
// use ReadDir for fs walk
//

import (
	"gitlab.com/jmmey/go-dirlog/cmd"
	"gitlab.com/jmmey/go-dirlog/lib"
)

func main() {

	// handle os.signals like SIGTERM SIGPIPE
	go lib.HandleSignals()

	// initialize cmd args
	cmd.Execute()

}
