package lib

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

type configEntry struct {
	Path  string
	Depth int
}

func readConfig(path string) []configEntry {
	f, err := os.Open(path)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	reader := bufio.NewScanner(f)

	var config []configEntry

	for reader.Scan() {
		var entry configEntry
		line := strings.Split(reader.Text(), " ")

		if len(line) == 2 {
			entry.Path = line[0]
			entry.Depth, err = strconv.Atoi(line[1])
		} else {
			log.Fatalf("ERROR: can't parse config file at: %s", line)
		}

		if err != nil {
			log.Fatal(err)
		}

		config = append(config, entry)

	}

	if err := reader.Err(); err != nil {
		log.Fatal(err)
	}

	return config

}
