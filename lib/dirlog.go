package lib

import (
	"fmt"
	"strconv"
	"time"
)

type dlog struct {
	_        [2]byte   /* padding */
	Status   uint16    /* 0 = NEWDIR, 1 = NUKE, 2 = UNNUKE, 3 = DELETED */
	Uptime   int32     /* Creation time since epoch (man 2 time) */
	Uploader uint16    /* The userid of the creator */
	Group    uint16    /* The groupid of the primary group of the creator */
	Files    uint16    /* The number of files inside the dir */
	_        [2]byte   /* padding */
	Bytes    uint64    /* The number of bytes in the dir */
	Dirname  [256]byte /* The name of the dir (fullpath) (max 255) */
	_        [8]byte   /* Unused, kept for compatibility reasons (max 8) */
}

var Dirlog dlog

func buildDirlogEntry(entry DirEntry) *dlog {
	var s_status uint16 = 0
	s_uptime := int32(entry.ModTime.Unix())
	var s_uploader uint16 = 100
	var s_group uint16 = 200
	var s_files uint16 = 0
	var s_bytes uint64 = 0

	// The folder name as a byte slice
	s_dirname := []byte("/" + entry.Name)

	s := &dlog{
		Status:   s_status,
		Uptime:   s_uptime,
		Uploader: s_uploader,
		Group:    s_group,
		Files:    s_files,
		Bytes:    s_bytes,
	}

	// copy the string into a slice of the byte array
	// https://stackoverflow.com/a/8045921
	copy(s.Dirname[:], s_dirname)

	return s
}

func RebuildDirlog(configPath string, writePath string, glRoot string) {

	config := readConfig(configPath)

	// List directories recursively
	var results []DirEntry

	for _, dir := range config {
		startTime := time.Now()
		fmt.Printf("Starting to process: %s with depth: %s\n", dir.Path, strconv.Itoa(dir.Depth))
		results = listDirRecursive(dir.Path, results, dir.Depth, 0, glRoot)
		fmt.Printf("Finished processing: %s in %s\n", dir.Path, processingTime(startTime).Round(time.Second))
	}

	// Build dirlog from results
	var dirlog []*dlog

	for _, dir := range results {

		dirlog = append(dirlog, buildDirlogEntry(dir))
	}

	writeFile(writePath, dirlog)

}
