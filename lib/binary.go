package lib

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"time"
)

func ReadFile(path string) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal("Error while opening file", err)
	}

	defer file.Close()

	for {
		data, err := readNextBytes(file, 288)
		if err == io.EOF {
			break
		}

		buffer := bytes.NewBuffer(data)

		err = binary.Read(buffer, binary.LittleEndian, &Dirlog)

		if err != nil {
			log.Fatal("binary.Read failed: ", err)
		}

		fmt.Printf("DIRLOG: %s - %s in %s files - created %s by %s.%s\n", string(bytes.Trim(Dirlog.Dirname[:], "\x00")), ByteCountBinary(int64(Dirlog.Bytes)), strconv.Itoa(int(Dirlog.Files)), time.Unix(int64(Dirlog.Uptime), 0), strconv.Itoa(int(Dirlog.Uploader)), strconv.Itoa(int(Dirlog.Group)))
	}
}

func writeFile(path string, data []*dlog) {
	file, err := os.Create(path)

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	for _, i := range data {
		var bin_buf bytes.Buffer
		binary.Write(&bin_buf, binary.LittleEndian, i)
		writeNextBytes(file, bin_buf.Bytes())
	}

}
