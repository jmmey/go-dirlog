package lib

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func HandleSignals() {

	signalChanel := make(chan os.Signal, 1)

	signal.Notify(signalChanel,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
		syscall.SIGPIPE)

	exit_chan := make(chan int)
	go func() {
		for {
			s := <-signalChanel
			switch s {

			// kill -SIGHUP XXXX [XXXX - PID for your program]
			case syscall.SIGHUP:
				fmt.Println("Signal hang up triggered.")

			// kill -SIGINT XXXX or Ctrl+c  [XXXX - PID for your program]
			case syscall.SIGINT:
				fmt.Println("Exiting: Signal interrupt triggered.")
				exit_chan <- 0

			// kill -SIGTERM XXXX [XXXX - PID for your program]
			case syscall.SIGTERM:
				fmt.Println("Signal terminate triggered.")
				exit_chan <- 0

			// kill -SIGQUIT XXXX [XXXX - PID for your program]
			case syscall.SIGQUIT:
				fmt.Println("Signal quit triggered.")
				exit_chan <- 0

			// kill -SIGPIPE XXXX [XXXX - PID for your program]
			case syscall.SIGPIPE:
				exit_chan <- 0

			default:
				continue
			}
		}
	}()
	exitCode := <-exit_chan
	os.Exit(exitCode)

}
