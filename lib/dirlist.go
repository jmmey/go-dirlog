package lib

import (
	"fmt"
	"log"
	"os"
	"path"
	"time"
)

type DirEntry struct {
	Name    string
	ModTime time.Time
}

func listDirRecursive(name string, dirs []DirEntry, depth int, currentDepth int, chdir string) []DirEntry {
	pwd, _ := os.Getwd()
	if pwd != chdir {
		e := os.Chdir(chdir)
		if e != nil {
			log.Fatalf("ERROR: couldn't change directory to %s", chdir)
		}
	}

	d, err := os.Open(name)
	if err != nil {
		fmt.Println(err.Error())
		log.Fatal(err)
	}
	defer d.Close()

	files, err := d.Readdir(-1)
	if err != nil {
		fmt.Println(err.Error())
	}

	currentDepth++

	for _, f := range files {
		if f.IsDir() {
			var dir DirEntry
			dir.Name = path.Join(name, f.Name())
			dir.ModTime = f.ModTime()
			if currentDepth < depth {
				dirs = listDirRecursive(path.Join(name, f.Name()), dirs, depth, currentDepth, chdir)
			} else {
				dirs = append(dirs, dir)
			}
		}
	}

	return dirs

}
