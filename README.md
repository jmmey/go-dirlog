# go-dirlog

# Features
* read and output dirlog binary file
* read sections from config and build new dirlog

# Config file
see dirlog.conf-example

Comments in the config file are currently not supported.

```
site/MP3 2                  # this would find MP3/0101/RELEASE-NAME MP3/0201/RELEASE2-NAME
site/FLAC 2                 # same as above
site/GAMES 1                # this would find GAMES/RELEASE-NAME
site/ARCHIVE/GAMES 1        # this would find ARCHIVE/GAMES/RELEASE-NAME
```

# Usage

`./go-dirlog rebuild --glroot /glftpd --output /glftpd/ftp-data/logs/dirlog`

You can run it from cron, or however you like.



# Notes
### dirlog kaitai format

```
meta:
  id: dirlog
  file-extension: dirlog
  endian: le
seq:
  - id: block
    type: block
    repeat: expr
    repeat-expr: 10
types:
  block:
    seq:
      - id: padding1
        type: u2
      - id: status
        type: u2
      - id: uptime
        type: s4
      - id: uploader
        type: u2
      - id: group
        type: u2
      - id: files
        type: u2
      - id: padding2
        type: u2
      - id: bytes
        type: u8
      - id: dirname
        type: str
        encoding: ASCII
        size: 256
      - id: dummy2
        type: u8
```