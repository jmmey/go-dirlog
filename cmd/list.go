package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jmmey/go-dirlog/lib"
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "list content of dirlog",
	Run: func(cmd *cobra.Command, args []string) {
		dirlogPath, _ := cmd.Flags().GetString("input")
		lib.ReadFile(dirlogPath)
	},
}

func init() {
	rootCmd.AddCommand(listCmd)
	listCmd.PersistentFlags().StringP("input", "i", "", "write to output file")
	listCmd.MarkFlagRequired("input")
}
