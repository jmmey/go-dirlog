package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var (
	dirlogPath = "./ftp-data/logs/dirlog"
	configPath = "./dirlog.conf"
)

var rootCmd = &cobra.Command{
	Use:   "dirlog",
	Short: "dirlog utility - reading and rebuilding dirlog file",
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
	},
}

func Execute() {

	rootCmd.PersistentFlags().String("glroot", "", "glftpd jail root dir")
	rootCmd.MarkFlagRequired("glroot")

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
