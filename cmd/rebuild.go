package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jmmey/go-dirlog/lib"
)

var rebuildCmd = &cobra.Command{
	Use:   "rebuild",
	Short: "rebuild dirlog from config files",
	Run: func(cmd *cobra.Command, args []string) {
		glRoot, _ := cmd.Flags().GetString("glroot")
		writePath, _ := cmd.Flags().GetString("output")
		lib.RebuildDirlog(configPath, writePath, glRoot)
	},
}

func init() {
	rootCmd.AddCommand(rebuildCmd)
	rebuildCmd.PersistentFlags().String("output", "", "write to output file")
	rebuildCmd.MarkFlagRequired("output")
}
